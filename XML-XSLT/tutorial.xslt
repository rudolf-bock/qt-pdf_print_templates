<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Edited by XMLSpy� -->
<html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<body style="font-family:Arial;font-size:12pt;">
	<div style="border-style: solid; color: teal;">
			<xsl:for-each select="tutorials/tutorial">
				<div style="color:green;padding:4px">
					<span style="font-weight:bold; color:blue;">
						<xsl:value-of select="name"/>
					</span>
					- <xsl:value-of select="url"/>
				</div>
			</xsl:for-each>
		</div>
	</body>
</html>